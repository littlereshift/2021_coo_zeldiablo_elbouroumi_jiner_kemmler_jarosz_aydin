package Test;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;
import codeJeu.*;
import moteurJeu.Commande;
public class TestCollision {
    @Test
    /**
     * Methode qui test le mouvement du personnage vers un mur
     */
    public void testCollision(){
        Decor [][] lc =new Decor[16][16];
		for (int i = 0;i<15;i++){
			for (int j = 0;j<15;j++){
				lc [i][j] = new CaseVide();
			}
		}
        lc[11][10]=new CasePleine();
        JeuPerso j=new JeuPerso(lc);
        Personnage p=new Personnage("Perso1",10,10,j);
        Commande c=new Commande();
        c.droite=true;
        p.deplacer(c);
        assertEquals("Le personnage ne doit pas bouger",10, p.getPosX());
    }
    @Test
    /**
     * Methode qui sert à tester le mouvement du personnage
     */
    public void testSeDeplacer(){
        Decor [][] lc =new Decor[16][16];
		for (int i = 0;i<15;i++){
			for (int j = 0;j<15;j++){
				lc [i][j] = new CaseVide();
			}
		}
        JeuPerso j=new JeuPerso(lc);
        Personnage p=new Personnage("Perso1",10,10,j);
        Commande c=new Commande();
        c.droite=true;
        p.deplacer(c);
        assertEquals("Le personnage doit  bouger",11, p.getPosX());

    }
}
