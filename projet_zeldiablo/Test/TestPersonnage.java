package Test;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;
import codeJeu.*;
class TestPersonnage {

	@Test
	/**
	 * Methode qui test la creation du personnage
	 */
	public void testPositionPersonnage() {
		Decor[][] cases=new Decor[15][15];
		Personnage perso = new Personnage("Perso1",10,10,new JeuPerso(cases));
		assertEquals("le personnage doit �tre plac� en position 10",10,perso.getPosX());
		assertEquals("le personnage doit �tre plac� en position 10",10,perso.getPosY());

	}
	
	@Test
	/**
	 * Methode qui test la methode diminuerPv
	 */
	public void testDegatPersonnage() {
		Decor[][] cases=new Decor[15][15];
		Personnage perso = new Personnage("Perso2",10,10,new JeuPerso(cases));
		perso.diminuerPv(5);
		
		assertEquals("le personnage doit avoir 15 pv",15,perso.getPv());
	}
	
	@Test
	/**
	 * Methode qui test la methode diminuerPv avec des degats negatif
	 */
	public void testDegatNegatifPersonnage() {
		Decor[][] cases=new Decor[15][15];
		Personnage perso = new Personnage("Perso3",10,10,new JeuPerso(cases));
		perso.diminuerPv(-2);
		
		assertEquals("le personnage doit toujours avoir 20 pv",20,perso.getPv());
	}
	
	@Test
	/**
	 * Methode qui test le deplacement
	 */
	public void testDeplacementPersonnage() {
		Decor[][] cases=new Decor[15][15];
		Personnage perso = new Personnage("Perso4",10,10,new JeuPerso(cases));
		perso.deplacerPoint(3,4);
		
		assertEquals("le personnage doit �tre plac� en position 3",3,perso.getPosX());
		assertEquals("le personnage doit �tre plac� en position 4",4,perso.getPosY());
	}
	
	@Test
	/**
	 * M�thode qui test le deplacement avec des valeurs negatif
	 */
	public void testDeplacementNegatifPersonnage() {
		Decor[][] cases=new Decor[15][15];
		Personnage perso = new Personnage("Perso5",10,10,new JeuPerso(cases));
		perso.deplacerPoint(-3,4);
		
		assertEquals("le personnage doit �tre plac� en position 10",10,perso.getPosX());
		assertEquals("le personnage doit �tre plac� en position 10",10,perso.getPosY());
	}

}
