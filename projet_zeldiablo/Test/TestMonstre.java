package Test;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;
import codeJeu.*;
class TestMonstre {

	@Test
	/**
	 * M�thode qui test la cr�ation du monstre
	 */
	public void testPositionMonstre() {
		Decor[][] cases=new Decor[15][15];
		Monstre mon = new Monstre("Mon1",10,10,new JeuPerso(cases));	
		assertEquals("le monstre doit �tre plac� en position 10",10,mon.getPosX());
		assertEquals("le monstre doit �tre plac� en position 10",10,mon.getPosY());

	}
	
	@Test
	/**
	 * M�thode qui test la m�thode diminuerPv
	 */
	public void testDegatPersonnage() {
		Decor[][] cases=new Decor[15][15];
		Monstre mon = new Monstre("Mon2",10,10,new JeuPerso(cases));		
		mon.diminuerPv(5);
		
		assertEquals("le monstre doit avoir 15 pv",15,mon.getPv());
	}
	
	@Test
	/**
	 * M�thode qui test la m�thode diminuerPv avec des d�gats n�gatif
	 */
	public void testDegatNegatifPersonnage() {
		Decor[][] cases=new Decor[15][15];
		Monstre mon = new Monstre("Mon3",10,10,new JeuPerso(cases));	
		mon.diminuerPv(-2);
		
		assertEquals("le monstre doit toujours avoir 20 pv",20,mon.getPv());
	}
	
	@Test
	/**
	 * M�thode qui test le d�placement
	 */
	public void testDeplacementPersonnage() {
		Decor[][] cases=new Decor[15][15];
		Monstre mon = new Monstre("Mon4",10,10,new JeuPerso(cases));	
		mon.deplacer(3,4);
		
		assertEquals("le monstre doit etre place en position 3",3,mon.getPosX());
		assertEquals("le monstre doit etre place en position 4",4,mon.getPosY());
	}
	
	@Test
	/**
	 * M�thode qui test le d�placement avec des valeurs n�gatif
	 */
	public void testDeplacementNegatifPersonnage() {
		Decor[][] cases=new Decor[15][15];
		Monstre mon = new Monstre("Mon5",10,10,new JeuPerso(cases));	
		mon.deplacer(-3,4);
		
		assertEquals("le monstre doit etre place en position 7",7,mon.getPosX());
		assertEquals("le monstre doit etre place en position 14",14,mon.getPosY());
	}
}
