package Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;
import codeJeu.*;
public class TestAttaque {
    @Test
    /**
     * Methode qui test l'attaque d'un personnage sur un monstre
     */
    public void testAttaquePersonnage(){
        Decor [][] lc =new Decor[16][16];
		for (int i = 0;i<15;i++){
			for (int j = 0;j<15;j++){
				lc [i][j] = new CaseVide();
			}
		}
        JeuPerso j=new JeuPerso(lc);
        Personnage p=new Personnage("Perso1",14,14,j);
        Monstre m=new Monstre("Monstre1 ", 13, 14, j);
        int pvAvantAttaque=m.getPv();
        p.attaquerEntiter(m);

        assertEquals("Le monstre doit perdre des pv",m.getPv(),pvAvantAttaque-p.getDegats());
    }
    @Test
    /**
     * Methode qui test l'attaque d'un monstre
     */
    public void testAttaqueMonstre(){
        Decor [][] lc =new Decor[16][16];
		for (int i = 0;i<15;i++){
			for (int j = 0;j<15;j++){
				lc [i][j] = new CaseVide();
			}
		}
        JeuPerso j=new JeuPerso(lc);
        Personnage p=new Personnage("Perso2",14,14,j);
        Monstre m=new Monstre("Monstre2 ", 13, 14, j);
        int pvAvantAttaque=p.getPv();
        m.attaquer(p);
        assertEquals("Le monstre doit perdre des pv",p.getPv(),pvAvantAttaque-m.getDegats());
    }
    @Test
    /**
     * Methode qui test l'attaque en dehors de la portee
     */
    public void testAttaquePersonnageHorsPortee(){
        Decor [][] lc =new Decor[16][16];
		for (int i = 0;i<15;i++){
			for (int j = 0;j<15;j++){
				lc [i][j] = new CaseVide();
			}
		}
        JeuPerso j=new JeuPerso(lc);
        Personnage p=new Personnage("Perso3",14,14,j);
        Monstre m=new Monstre("Monstre3", 12, 14, j);
        int pvAvantAttaque=p.getPv();
        m.attaquer(p);
        assertEquals("Le personnage ne doit pas perdre de pv",p.getPv(),pvAvantAttaque);
    }
    @Test
    /**
     * Methode qui test l'attaque en dehors de la portee
     */
    public void testAttaqueMonstrHorsPortee(){
        Decor [][] lc =new Decor[16][16];
		for (int i = 0;i<15;i++){
			for (int j = 0;j<15;j++){
				lc [i][j] = new CaseVide();
			}
		}
        JeuPerso j=new JeuPerso(lc);
        Personnage p=new Personnage("Perso3",14,14,j);
        Monstre m=new Monstre("Monstre3", 12, 14, j);
        int pvAvantAttaque=m.getPv();
        p.attaquerEntiter(m);
        assertEquals("Le monstre ne doit pas perdre de pv",m.getPv(),pvAvantAttaque);
    }

}
