package codeJeu;
/**
 * Classe qui permet de créer une sortie
 */
public class Sortie implements Decor {
	
	// attributs

	private int posX;
	private int posY;
	
	// constructeur
	
	public Sortie(int x, int y) {
		posX=x ;
		posY=y ;
	}

	// methodes
	
	/**
	 * Methode utiliser pour positionner un personnage sur le 
	 * case sortie
	 * @param p personnage
	 */
	public void estSorti(Personnage p) {
		p.deplacerPoint(this.posX, this.posY) ;
	}
	//Getter
	/**
	 * Methode qui permet de retourner la position en X de la Sortie
	 * @return posX
	 */
	@Override
	public int getX() {
		return posX;
	}
	/**
	 * Methode qui permet de retourner la position en Y de la Sortie
	 * @return posY
	 */
	@Override
	public int getY() {
		return posY;
	}
	/**
	 * Methode qui permet de dire si la case est traversable ou pas
	 * @return true
	 */
	@Override
	public boolean estTraversable() {
		return true;
	}

	//methodes non utiliser
	@Override
	public void monstreVivant() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void monstreMort() {
		// TODO Auto-generated method stub
		
	}

}