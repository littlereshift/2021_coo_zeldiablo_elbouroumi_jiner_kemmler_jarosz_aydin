package codeJeu;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import moteurJeu.*;
/**
 * Interface qui sert à dessiner le jeu
 */
public class DessinPerso implements DessinJeu {


	// attributs

	/**
	 * Taille
	 */
	private static int TAILLE_CASE=25;
	
	/**
	 * JeuPerso
	 */
	private JeuPerso jeu;
	

	// constructeur

	/**
	 * Constructeur de DessinPerso
	 * @param j JeuPerso
	 */
	public DessinPerso(JeuPerso j) {
		this.jeu=j;
	}

	// methodes

	/**
	 * Methode qui dessine un objet
	 * @param s nom de l'objet
	 * @param x coordonnee en x
	 * @param y coordonnee en y
	 * @param im image
	 */
	private void dessinerObjet(String s, int x, int y, BufferedImage im) {
		Graphics2D crayon = (Graphics2D) im.getGraphics();
		switch (s) {
		case "PJ":
			crayon.setColor(Color.blue);
			crayon.fillOval(x * TAILLE_CASE, y * TAILLE_CASE, TAILLE_CASE,
					TAILLE_CASE);
			break;
		case "MUR":
			crayon.setColor(Color.gray);
			crayon.fillRect(x * TAILLE_CASE, y * TAILLE_CASE, TAILLE_CASE,
					TAILLE_CASE);
			break;			
		case "MON":
			crayon.setColor(Color.orange);
			crayon.fillOval(x * TAILLE_CASE, y * TAILLE_CASE, TAILLE_CASE,
					TAILLE_CASE);
		break;
		default:
			throw new AssertionError("objet inexistant");
		}
	}

	/**
	 * Methode qui dessine le jeu
	 * @param im image
	 */
	public void dessiner(BufferedImage im) {
		Personnage pj = jeu.getPj();
		ZeldiabloJeu zj=jeu.getZj();
		ArrayList<Monstre> lm=jeu.getListMon();
		this.dessinerLabyrinthe(jeu.getDecor(),im);
		if(!pj.etreMort()) {
			this.dessinerObjet("PJ", pj.getPosX(), pj.getPosY(), im);
		}
		for(int i=0;i<lm.size();i++){
			Monstre m=lm.get(i);
			if(!m.etreMort()) {
				this.dessinerObjet("MON", m.getPosX(), m.getPosY(), im);
			}
		}
	}

	/**
	 * Methode qui dessine le labyrinthe
	 * @param c labyrinthe
	 * @param im image
	 */
	public void dessinerLabyrinthe(Decor[][] c,BufferedImage im){
		Graphics2D crayon = (Graphics2D) im.getGraphics();
		for(int i=0;i<20;i++){
			for(int j=0;j<20;j++){
				if(c[j][i] instanceof CaseVide){
					crayon.setColor(Color.LIGHT_GRAY);
					crayon.fillRect(i*TAILLE_CASE, j * TAILLE_CASE,TAILLE_CASE,TAILLE_CASE);
				}
				if(c[j][i] instanceof CasePleine){
					crayon.setColor(Color.GRAY);
					crayon.fillRect(i*TAILLE_CASE, j * TAILLE_CASE,TAILLE_CASE,TAILLE_CASE);
				}
				if(c[j][i] instanceof Entree){
					crayon.setColor(Color.green);
					crayon.fillRect(i*TAILLE_CASE, j * TAILLE_CASE,TAILLE_CASE,TAILLE_CASE);
				}
				if(c[j][i] instanceof Sortie){
					crayon.setColor(Color.red);
					crayon.fillRect(i*TAILLE_CASE, j * TAILLE_CASE,TAILLE_CASE,TAILLE_CASE);
				}
			}
		}
	}
}
