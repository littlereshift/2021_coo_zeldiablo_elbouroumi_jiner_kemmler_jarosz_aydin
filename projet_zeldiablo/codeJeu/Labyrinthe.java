package codeJeu;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
/**
 * Classe qui permet de créer un labyrinthe
 */
public class Labyrinthe {
	//Attributs

	private JeuPerso jeu;
	private File fichier;
	public Decor[][] CaseLab;
	private Personnage perso;
	private boolean fini;
	
	/**
	 * Constructeur sans personnage
	 * @param dec
	 */

	public Labyrinthe() {
		jeu=new JeuPerso(this.creationLabyrinthe("projet_zeldiablo/Labyrinthe/Labyrinthe.txt"));
		fini=false;
		//fichier=new File(	);
		}
	
	/**
	 * Methode qui cree un tableau de decor en lisant un fichier .txt
	 * @param nomF nom du fichier
	 * @return un tableau de decor
	 */
	public static Decor[][] creationLabyrinthe(String nomF) {
		Decor[][] po=new Decor[20][20];
		try {
			BufferedReader s=new BufferedReader(new FileReader(nomF));
			for(int i=0;i<20;i++) {
				for(int j=0;j<20;j++) {
					char labi =(char)s.read();
					switch(labi){
						case 'V':
						po[i][j]=new CaseVide();
						break;
						case 'M':
						CaseVide c=new CaseVide();
						c.monstreVivant();
						po[i][j]=c;
						break;
						case 'W':
						po[i][j]=new CasePleine();
						break;
						case 'D':
						po[i][j]=new Entree(i,j);
						break;
						case 'F':
						po[i][j]=new Sortie(i,j);
						break;
					}
				}
				s.read();
				s.read();
			}
		s.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}catch(IOException ioe){
		}
		return po;
	}

	// getter
	/**
	 * Methode qui permet de retourner le Decor du labyrinthe
	 * @return CaseLab le decor
	 */
	public Decor[][] getDecor(){
		return CaseLab;
	}
	/**
	 * Methode qui permet de retourner le fichier lu pour créer le labyrinthe
	 * @return fichier le fichier
	 */
	public File getFichier(){
		return fichier;
	}
	/**
	 * Methode qui permet de set le Decor du labyrinthe
	 * @param a Le decor donné
	 */
	public void setDecor(Decor[][] a){
		CaseLab=a;
	}
	/**
	 * Methode qui donne le type de case en fonction de la position
	 * @param x la position en x de la case
	 * @param y La position en y de la case
	 * @return Le type de la case
	 */
	public Decor getDecor(int x, int y) {
		return this.CaseLab[x][y] ;
	}
}
