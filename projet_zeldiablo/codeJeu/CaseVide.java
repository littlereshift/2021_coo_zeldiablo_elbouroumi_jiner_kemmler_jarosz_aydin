package codeJeu;
/**
 * La classe CaseVide sert à savoir si le personnage pourra traverser ou pas cette case.
 * Si le Decor est une CaseVide, cela signifie qu'il s'agit d'une case où le personnage peut passer.
 */ 
public class CaseVide implements Decor {

	// attributs

	private int positionX;
	private int positionY;
	private boolean monstre;

	// constructeur

	public CaseVide() {

	}
	
	// methodes
	
	/**
	 * Methode qui indique si un monstre est sur la casse
	 */
	public void monstreVivant(){
		monstre=true;
	}
	
	/**
	 * Methode qui indique si le monstre est mort
	 */
	public void monstreMort(){
		monstre=false;
	}
	
	/**
	 * estTraversable permet retourne un booleen qui va nous permettre de savoir si le personnage peut aller sur cette case
	 * @return la case est traversable
	 */ 
	@Override
	public boolean estTraversable() {
		if(monstre) {
			return false;
		}else {
			return true;
		}
	}
	
	// getters

	/**
	 * getX permet de connaitre la position de la CaseVide sur l'axe des abscisses
	 * @return position sur l'axe des abscisses de la CaseVide
	 */
	@Override
	public int getX() {
		return positionX;
	}
	
	/**
	 * getY permet de connaitre la position de la CaseVide sur l'axe des ordonn�es
	 * @return position sur l'axe des ordonn�es de la CaseVide
	 */
	@Override
	public int getY() {
		return positionY;
	}


}
