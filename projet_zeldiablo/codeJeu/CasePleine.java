package codeJeu;
/**
 * La classe CasePleine sert à savoir si le personnage pourra traverser ou pas cette case.
 * Si le Decor est une CasePleine, cela signifie qu'il s'agit d'un mur.
 */  

public class CasePleine implements Decor {

	// attributs
	
	private int posX;
	private int posY;

	// constructeur

	public CasePleine() {
		
	}
	
	// methodes
	
	/**
	 * estTraversable permet retourne un booléen qui va nous permettre de savoir si le personnage peut aller sur cette case
	 * @return la case n'est pas traversable
	 */ 
	@Override
	public boolean estTraversable() {
		return false;
	}

	
	// getters
	
	/**
	 * getX permet de connaître la position de la CasePleine sur l'axe des abscisses
	 * @return position sur l'axe des abscisses de la CasePleine
	 */
	@Override
	public int getX() {
		return posX;
	}
	
	/**
	 * getY permet de connaître la position de la CasePleine sur l'axe des ordonnées
	 * @return position sur l'axe des ordonnées de la CasePleine
	 */
	@Override
	public int getY() {
		return posY;
	}

	//methodes non utilisees
	
	@Override
	public void monstreVivant() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void monstreMort() {
		// TODO Auto-generated method stub
		
	}

}
