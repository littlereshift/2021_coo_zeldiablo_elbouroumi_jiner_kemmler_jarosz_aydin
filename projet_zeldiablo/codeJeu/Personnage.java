package codeJeu;

import java.util.ArrayList;

import moteurJeu.Commande;
/**
 * Classe qui permet de créer un personnage
 */
public class Personnage implements Entite{

	// attributs 

	private String nom;
	private int posX;
	private int posY;
	private int pv;
	private int degat;
	final static int LIMIT_X=20;
	final static int LIMIT_Y=20;
	private JeuPerso jeu;
	private static Labyrinthe labyrinthe;
	
	/**
	 * Constructeur vide pour le personnage
	 */
	public Personnage() {
		nom="Perso";
		posX=0;
		posY=0;
		pv=20;
		degat=5;
	}
	/**
	 * Constructeur d'un personnage en fonction d'un labyrinthe
	 * @param n Le nom du personnage
	 * @param x La position en X dans le Labyrinthe
	 * @param y La position en Y dans le labyrinthe
	 * @param l Le labyrinthe affecté au Personnage
	 */
	public Personnage(String n,int x, int y,Labyrinthe l) {
		nom=n;
		posX=x;
		posY=y;
		labyrinthe=l;
		degat=5;
		pv=20;
	}
	/**
	 * Constructeur d'un personnage en fonction d'un Labyrinthe
	 * @param n Le nom du personnage
	 * @param a La position en x du Labyrinthe
	 * @param b La position en y du Labyrinthe
	 * @param j Le jeu affecté au Personnage
	 */
	public Personnage(String n,int a, int b, JeuPerso j){
		nom=n;
		posX=a;
		posX=b;
		jeu=j;
		degat=5;
		pv=20;
	}
	
	/**
	* Methode qui permet de deplacer le personnage a un point donne
	* @param x , represente la coordonnee de posX de la colonne
	* @param y , represente la coordonnee de posY de la colonne
	*/
	public void deplacerPoint(int x, int y) {
    	this.posX=x;
    	this.posY=y;
	}
	
	/**
	 * Methode permettant de savoir un personnage est mort
	 * @return un boolean indiquant son etat
	 */
	public boolean etreMort(){
		if(this.pv<=0){
			return true;
		}else{
			return false;
		}
	}
	/**
	 * Methode qui diminuer les points de vie du personnage
	 * @param degat , degats subit par le personnage
	 */
	public void diminuerPv(int degat) {
		if(degat>0) {
			pv=-degat;
		}	
	}

		/**
	 * Methode qui permet de deplacer le personnage
	 * @param c , represente la commande du joueur
	 */
	public void deplacer(Commande c) {
		Decor[][] d=jeu.getDecor();
			if(c.gauche) {
				if(this.posX<0) {
					this.posX=0;
				} else {
					if(posY>=0 && posX-1>=0 && posY<20 && posX-1<20) {
						if (d[posY][posX-1].estTraversable()) {
							this.posX--;
						}
					}
				}
			}
			if(c.droite) {
				if(this.getPosX() >LIMIT_X) {
					this.setPosX(LIMIT_X);
				}
				else {
					if(posY>=0 && posX+1>=0 && posY<20 && posX+1<20) {
						if (d[posY][posX+1].estTraversable()) {
							this.posX++;
						}	
					}
				}
			}
			if(c.haut) {
				if(this.getPosX()<0) {
					this.setPosY(0);
				} else {
					if(posY-1>=0 && posX>=0 && posY-1<20 && posX<20) {
						if (d[posY-1][posX].estTraversable()) {
							this.posY--;
						}	
					}
				}
			}
			if(c.bas) {
				if(this.getPosY()>LIMIT_Y) {
					this.setPosY(LIMIT_Y);
				} else {
					if(posY+1>=0 && posX>=0 && posY+1<20 && posX<20) {
						if (d[posY+1][posX].estTraversable()) {
							this.posY++;
						}
					}
					
				}
			}
		
	}
	
	/**
	 * Methode attaquer, lorsque la commande attaquer est enclancher 
	 * on parcourt la list de monstre et le monstre ayant une distance = 1 ce fait attaquer
	 * @param commande ,commande effectuer
	 */
	public void attaquer(Commande commande) {
		if(commande.attaquer) {
			ArrayList<Monstre> m=jeu.getListMon();
			for(int i=0;i<m.size();i++) {
				Monstre mon=m.get(i);
				attaquerEntiter(mon);
			}	
		}
	}
	
  	 /**
  	  * Methode qui attaque une entiter
  	  * @param victime entiter qui vas subir des degats
  	  */
	public void attaquerEntiter(Entite victime) {
		int degat=0 ;
		degat=this.getDegats() ;
		if (this.pv!=0) {
			if (this.getDistance(victime)<=1 || this.getDistance(victime)==-1){
				if (victime.getPv()>0) {
					victime.diminuerPv(degat) ;
				}
			}
		}
	}

	/**
	 * Methode qui permet de trouver la distance entre le personnage et sa victime
	 * @param victime recherchee
	 * @return distance 
	 */
  	public int getDistance(Entite victime) {
    	int distance ;
    	distance=Math.abs(victime.getPosX()-this.posX)+Math.abs(victime.getPosY()-this.posY) ;    	
    	return distance ;
  	}
	
	
	//Getter & Setter
	
	//nom
	/**
	 * @return nom
	 */
	public String getNom() {
		return nom;
	}
	
	/**
	 * @param n
	 */
	public void setNom(String n) {
		nom=n;
	}
	
	//x
	/**
	 * @return posx
	 */
	public int getPosX() {
		return posX;
	}
	
	/**
	 * @param x
	 */
	public void setPosX(int x) {
		posX=x;
	}
	
	//y
	/**
	 * @return posy
	 */
	public int getPosY() {
		return posY;
	}
	
	/**
	 * @param y
	 */
	public void setPosY(int y) {
		posY=y;
	}
	
	//pv
	/**
	 * @return pv
	 */
	public int getPv() {
		return pv;
	}
	
	/**
	 * @param p
	 */
	public void setPv(int p) {
		pv=p;
	}
	
	//degat
	/**
 	* @return degat
 	*/
	public int getDegats() {
		return degat;
	}
	
	/**
	 * @param d
	 */
	public void setDegats(int d) {
		degat=d;
	}

	
	//labyrinthe
	/**
	 * @return labyrinthe
	 */
	public Labyrinthe getLabyrinthe() {
		return labyrinthe;
	}
	
	/**
	 * @param l
	 */
	public void setLabyrinthe(Labyrinthe l) {
		labyrinthe=l;
	}


}
