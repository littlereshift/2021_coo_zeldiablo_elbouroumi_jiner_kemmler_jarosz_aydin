package codeJeu;
import moteurJeu.*;
/**
 * Classe permettant de générer un jeu Zeldiablo
 */
public class ZeldiabloJeu {
	//Attributs
	private Labyrinthe jeu;
	private Personnage personnage;

	/**
	 * Constructeur d'un jeu Zeldiablo
	 * @param j Le personnage
	 */
	public ZeldiabloJeu(Personnage j) {
		personnage=j;
		
	}
	/**
	 * Methode qui permet de retourner le Decor du jeu 
	 * @return le decor
	 */
	public Decor[][] getDecor(){
		return jeu.getDecor();
	}
}
