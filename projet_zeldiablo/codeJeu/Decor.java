package codeJeu;
/**
 * Interface qui sert à créer des entite inerte
 */
public interface Decor {

	// Attributs

	/*
	 * position en x
	 */
	int posx=0;
	
	/**
	 * position en y
	 */
	int posy=0;
	
	/**
	 * Methode permettant de savoir si la case est traversable
	 * @return un boolean
	 */
	boolean estTraversable();
	
	/**
	 * Methode qui ajout un monstre
	 */
	void monstreVivant();
	
	/**
	 * Methode qui supprime un monstre d'une case
	 */
	void monstreMort();	
	
	
	//getter
	/**
	 * Methode qui retourne la position en X du Decor
	 * @return X
	 */
	int getX();
	/**
	 * Methode qui retourne la position en Y du Decor
	 * @return Y
	 */
	int getY();
		
	
}
