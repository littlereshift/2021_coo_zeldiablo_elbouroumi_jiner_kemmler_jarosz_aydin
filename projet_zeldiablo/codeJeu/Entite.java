package codeJeu;
import moteurJeu.*;
/**
 * Interface qui permet de créer des entite vivantes
 */
public interface Entite {
	
	//Methodes

	/**
	 * Methode qui diminuer les points de vie 
	 * @param degat , degats subit 
	 */
	public void diminuerPv(int degat);
	
	/**
	 * Methode qui permet de deplacer le personnage
	 * @param x , represente la coordonnee x, de la colonne
	 * @param y , represente la coordonnee y, de la ligne
	 */
	public void deplacer(Commande c);

	//getter et setter

	/**
	 * Getter qui permet de récupérer les pv d'une entite
	 * @return pv
	 */
	public int getPv();
	/**
	 * Setter qui permet d'affecter des PV à une entite
	 * @param i entier à donner
	 */
	public void setPv(int i);
	
	/**
	 * Getter qui permet de récupérer les dégats d'une entite
	 * @return Degats les degates d'une entite
	 */
	public int getDegats();

	/**
	 * Getter qui permet de donner la distance entre deux entites
	 * @param victime victime que l'entite cherche à viser
	 * @return la distance entre les deux entites
	 */

	public int getDistance(Entite victime);

	/**
	 * Getter de la position en X d'une entite
	 * @return PosX position sur l'axe des abscisses dans à la quelle se trouve l'entite
	 */
	public int getPosX();
	/**
	 * Getter de la position en Y d'une entite
	 * @return PosY position sur l'axe des ordonnées dans à la quelle se trouve l'entite
	 */
	public int getPosY();
	/**
	 * Getter qui donne le Labyrinthe dans lequel l'entite est
	 * @return Labyrinthe dans le quel est le personnage
	 */
	public Labyrinthe getLabyrinthe();
}
