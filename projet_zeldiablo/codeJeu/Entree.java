package codeJeu;
/**
 * Classe qui permet de créer une Entree
 */
public class Entree implements Decor {
	
	// attributs

	private int posX;
	private int posY;
	
	// constructeur
	
	public Entree(int x, int y) {
		posX=x ;
		posY=y ;
	}

	// methodes
	
	/**
	 * Methode qui permet de positionner un personnage
	 * sur la case entree
	 * @param p
	 */
	public void entre(Personnage p) {
		p.deplacerPoint(this.posX, this.posY) ;
	}

	/**
	 * Methode qui permet de récupérer la position en X de l'entree
	 * @return posX
	 */
	@Override
	public int getX() {
		return posX;
	}
	/**
	 * Methode qui permet de récupérer la position en X de l'entree
	 * @return posX
	 */
	@Override
	public int getY() {
		return posY;
	}
	/**
	 * Methode qui permet de dire si la case est traversable ou pas
	 * @return true
	 */
	@Override
	public boolean estTraversable() {
		return true;
	}

	
	//methodes non utiliser
	@Override
	public void monstreVivant() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void monstreMort() {
		// TODO Auto-generated method stub
		
	}



}