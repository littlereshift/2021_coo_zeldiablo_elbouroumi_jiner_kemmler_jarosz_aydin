package codeJeu;
import java.util.ArrayList;
import moteurJeu.Commande;
import moteurJeu.Jeu;
/**
 * Classe qui permet de créer un Jeu
 */
public class JeuPerso implements Jeu{
	//Attributs
	/**
	 * le personnage du jeu
	 */
	private Personnage pj;

	/**
	 * Zeldiablo
	 */
	private ZeldiabloJeu zj;

	/**
	 * Tableau de decor
	 */
	private Decor[][] dec;

	/**
	 * Liste Monstre
	 */
	private ArrayList<Monstre> lm;

	/**
	 * constructeur de jeu avec un Personnage
	 */
	public JeuPerso() {
		this.pj=new Personnage();
		zj=new ZeldiabloJeu(pj);
		lm=new ArrayList<Monstre>();
	}

	/**
	 * Constructeur
	 * @param cases tableau de decor
	 */
	public JeuPerso(Decor[][] cases){
		boolean entree=false;
		dec=cases;
		int i=0;
		int j=0;
		pj=new Personnage("Jean",i,j,this);
		//Monstre
		lm=listMonstre(cases);
	}
	
	
	/**
	 * Methode qui cree une list de monstre grace a l'initialisation d'un tableau Decor
	 * @param d tableau de decor
	 * @return liste de monstre
	 */
	public ArrayList<Monstre> listMonstre(Decor[][] d){
		ArrayList<Monstre> mon=new ArrayList<Monstre>();
		for(int i=0;i<20;i++) {
			for(int j=0;j<20;j++) {
				Decor dec=d[i][j];
				if(dec instanceof CaseVide && !dec.estTraversable()) {
					Monstre m=new Monstre("Monstre",j,i,this);
					mon.add(m);
				}
			}
		}
		return mon;
	}
	
	/**
	 * surcharge toString
	 */
	public String toString() {
		return ("" + this.getPj());
	}

	/**
	 * demande a deplacer le personnage
	 * 
	 * @param commande
	 *            chaine qui donne ordre
	 */
	public void evoluer(Commande commande) {
		this.getPj().deplacer(commande);
		this.getPj().attaquer(commande);
		for(int i=0;i<lm.size();i++) {
			Monstre mon=lm.get(i);
			if(mon.etreMort()) {
				dec[mon.getPosY()][mon.getPosX()].monstreMort();
			}
		}
	}

	@Override
	public boolean etreFini() {
		// le jeu n'est jamais fini
		return false;
	}

	/**
	 * getter pour l'affichage
	 * 
	 * @return personnage du jeu
	 */
	public Personnage getPj() {
		return pj;
	}

	/**
	 * Getter de Zeldiablo
	 * @return zeldiablo
	 */
	public ZeldiabloJeu getZj(){
		return zj;
	}

	/**
	 * Getter de tableau de decor
	 * @return tableau decor
	 */
	public Decor[][] getDecor(){
		return dec;
	}

	/**
	 * Getter de liste
	 * @return liste monstre
	 */
	public ArrayList<Monstre> getListMon(){
		return lm;
	}

}
