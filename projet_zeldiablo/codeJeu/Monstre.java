package codeJeu;

import moteurJeu.Commande;

public class Monstre implements Entite{
	/**
 	* Classe qui permet de créer un Monstre
 	*/

	 // attributs 

	private String nom;
	private int posX;
	private int posY;
	private int pv;
	private int degat;
	final static int LIMIT_X=20;
	final static int LIMIT_Y=20;
	private JeuPerso jeu;
	private Labyrinthe labyrinthe;

	// constructeurs
	
	public Monstre(String n,int x, int y,Labyrinthe l) {
		nom=n;
		posX=x;
		posY=y;
		labyrinthe=l;
		pv=20;
	}

	public Monstre(String n,int x, int y,JeuPerso j) {
		nom=n;
		posX=x;
		posY=y;
		jeu=j;
		pv=20;
	}
	
	/**
	 * Methode qui retourne true si le monstre est mort
	 * @return l'etat du monstre
	 */
	public boolean etreMort(){
		if(this.pv<=0){
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * Methode qui diminuer les points de vie du monstre
	 * @param degat , degats subit par le monstre
	 */
	public void diminuerPv(int degat) {
		if(degat>0) {
			pv=-degat;
		}
	}
	
	
	/**
	 * Methode qui permet de deplacer le personnage
	 * @param x , represente la coordonnee x, de la colonne
	 * @param y , represente la coordonnee y, de la ligne
	 */
	public void deplacer(int x, int y) {
		if(x>0 && y>0) {
			posX=x;
			posY=y;
		}
	}

	/**
 	  * Methode qui attaque une entiter
 	  * @param victime entiter qui vas subir des degats
 	  */
	public void attaquer(Entite victime) {
		int degat=0 ;
		degat=this.getDegats() ;
		if (this.pv!=0) {
			if (victime.getLabyrinthe()==this.labyrinthe) {
				if (this.getDistance(victime)<=1){
					if (victime.getPv()>0) {
						victime.diminuerPv(degat) ;
					}
				}
			}
		}
	}
	
	/**
	 * Methode qui permet de trouver la distance entre le monstre et sa victime
	 * @param victime recherchee
	 * @return distance 
	 */
	public int getDistance(Entite victime) {
		int distance ;
		distance=victime.getPosX()-this.posX+victime.getPosY()-this.posY ;
		return distance ;
	}
	
	
	//Getter & Setter
	
	/**
	 * Getter qui donne le nom du monstre
	 * @return nom 
	 */
	public String getNom() {
		return nom;
	}
	/**
	 * Setter qui permet d'affecter un nom à un monstre
	 * @param n le nouveau nom
	 */
	public void setNom(String n) {
		nom=n;
	}
	
	/**
	 * Methode qui permet de récupérer la position en X du monstre
	 * @return posX 
	 */
	public int getPosX() {
		return posX;
	}
	/**
	 * Methode qui permet d'affecter à un monstre sa position en X
	 * @param x La position en X
	 */
	public void setPosX(int x) {
		posX=x;
	}
	
	/**
	 * Methode qui permet de récupérer la position en Y du monstre
	 * @return posY 
	 */
	public int getPosY() {
		return posY;
	}
	/**
	 * Methode qui permet d'affecter à un monstre sa position en Y
	 * @param y La position en Y
	 */
	public void setPosY(int y) {
		posY=y;
	}
	
	/**
	 * Methode qui permet de récupérer les PV du monstre
	 * @return posY 
	 */
	public int getPv() {
		return pv;
	}
	/**
	 * Methode qui permet d'affecter à un monstre un certain nombre de pv
	 * @param p Les pv
	 */
	public void setPv(int p) {
		pv=p;
	}
	
	/**
	 * Methode qui permet de récupérer les degats du monstre
	 * @return degat
	 */
	public int getDegats() {
		return degat;
	}
	/**
	 * Methode qui permet d'affecter à un monstre un certain nombre de degats
	 * @param d Les degats
	 */
	public void setDegat(int d) {
		degat=d;
	}
	
	/**
	 * Methode qui permet de récupérer le labyrinthe du monstre
	 * @return Labyrinthe
	 */
	public Labyrinthe getLabyrinthe() {
		return labyrinthe;
	}
	/**
	 * Methode qui permet d'affecter à un monstre un certain Labyrinthe
	 * @param l le labyrinthe
	 */
	public void setLabyrinthe(Labyrinthe l) {
		labyrinthe=l;
	}

	//Methode non utiliser
	public void deplacer(Commande c) {
		
	}


	
}
